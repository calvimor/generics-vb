﻿Public Class MyCollection(Of T)
    Private _data As New ArrayList()

    Public Sub Add(value As T)
        _data.Add(value)
    End Sub

    Public ReadOnly Property Count As Integer
        Get
            Return _data.Count
        End Get
    End Property

    Default Public Property Item(index As Integer) As T
        Get
            Return CType(_data(index), T)
        End Get
        Set(value As T)
            _data(index) = value
        End Set
    End Property



End Class
