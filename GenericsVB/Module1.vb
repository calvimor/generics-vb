﻿Option Strict On
Imports System.Collections.Generic

Module Module1

    Sub Main()
        Dim numbers As New IntCollection()

        numbers.Add(2)
        numbers.Add(3)
        numbers.Add(5)
        numbers.Add(8)
        numbers.Add(13)
        numbers.Add(21)

        ' numbers.Add("hello")

        Dim sum = 0
        For i = 0 To numbers.Count - 1
            sum += numbers(i)
        Next
        Console.WriteLine(sum)


        Dim fibs() As Integer = {2, 3, 5, 8, 13, 21}
        Dim numbers2 As New List(Of Integer)
        Dim employees As New List(Of Employee)
        Dim employeeDict As New Dictionary(Of Integer, Employee)

        For i = 0 To fibs.Length - 1
            Dim fib = fibs(i)
            numbers2.Add(fib)

            Dim emp As New Employee(fib, "Employee " & fib, True)
            employees.Add(emp)
            employeeDict.Add(emp.Id, emp)
        Next

        Console.WriteLine("Numbers:")
        For i = 0 To numbers2.Count - 1
            Console.WriteLine(numbers2(i))
        Next

        Console.WriteLine("")
        Console.WriteLine("Employees:")
        For i = 0 To employees.Count - 1
            Console.WriteLine(employees(i).Id)
        Next

        Console.WriteLine("")
        Console.WriteLine("Employee Dictonary:")
        For i = 2 To 21
            Dim value = "not in the collection"
            If employeeDict.ContainsKey(i) Then
                value = employeeDict(i).Name
            End If
            Console.WriteLine("The employee with key {0} is {1}", i, value)
        Next

        ' Generic interface
        Dim rand As New Random()
        Dim shapes As New List(Of Shape)
        For i = 1 To 10
            Dim shape As Shape
            Dim name = "Shape" & i
            Dim value = rand.Next(1, 20)
            If i Mod 2 = 0 Then
                shape = New Square(id:=i, name:=name, side:=value)
            Else
                shape = New Circle(id:=i, name:=name, radius:=value)
            End If
            shapes.Add(shape)
        Next

        shapes.Sort()

        For Each shape In shapes
            Console.WriteLine(shape.Name & ": " & shape.Area)

        Next


    End Sub

End Module

