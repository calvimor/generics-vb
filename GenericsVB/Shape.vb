﻿Public MustInherit Class Shape
    Implements IXmlExport
    Implements IComparable(Of Shape)

    Public Property Id As Integer
    Public Property Name As String
    Public MustOverride ReadOnly Property Area As Double

    Public MustOverride Function GetXmlAsElement() As XElement Implements IXmlExport.GetXmlAsElement

    Public Overridable Function GetXmlAsString() As String Implements IXmlExport.GetXmlAsString
        Dim element = GetXmlAsElement()
        Return element.ToString()
    End Function

    Public Function CompareTo(other As Shape) As Integer Implements IComparable(Of Shape).CompareTo

        Dim result = 0
        If other Is Nothing OrElse Me.Area > other.Area Then
            result = 1
        ElseIf other.Area > Me.Area Then
            result = -1
        End If

        Return result
    End Function
End Class
