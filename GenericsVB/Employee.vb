﻿Public Class Employee
    Implements IXmlExport

    Private _name As String
    Private _birthDate As DateTime

    Public Property Name As String
        Get
            Return _name
        End Get
        Set(value As String)
            If Not String.IsNullOrEmpty(value) Then
                _name = value
            End If
        End Set
    End Property

    Public ReadOnly Property Age As Integer
        Get
            Dim diff = DateTime.Now - _birthDate
            Return diff.Days \ 365
        End Get
    End Property

    Public Property Id As Integer
    Public Property Salaried As Boolean

    Public Sub New()
        Name = "<empty>"
        Salaried = False
    End Sub

    Public Sub New(ByVal id As Integer,
        ByVal name As String, _
        ByVal salaried As Boolean)

        Me.Id = id
        Me.Name = name
        Me.Salaried = salaried
    End Sub

    Public Function GetDescription()
        Return String.Format( _
            "{0} is{1} a salaried employee", _
            Name, IIf(Salaried, "", " not"))
    End Function

    Public Function GetXmlAsString() As String Implements IXmlExport.GetXmlAsString
        Dim element = GetXmlAsElement()
        Return element.ToString()
    End Function

    Public Function GetXmlAsElement() As XElement Implements IXmlExport.GetXmlAsElement
        Dim element As New XElement("Employee", _
            New XAttribute("Id", Id), _
            New XAttribute("Name", Name), _
            New XAttribute("Salaried", Salaried))
        Return element
    End Function
End Class
